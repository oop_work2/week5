package com.phichet.week;

public class BubbleSortApp {
    public static void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public static void bubble(int[] arr, int first, int second) {
        for (int i = first;i < second;i++){
            if(arr[i]>arr[i+1]){
                swap(arr, i, i+1);
            }
        }
    }

    public static void bubbleSort(int[] arr) {
        for (int i = 4;i>0;i--){
            bubble(arr , 0, i);
        }
    }
}
