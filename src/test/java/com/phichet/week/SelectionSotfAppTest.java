package com.phichet.week;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class SelectionSotfAppTest {
    @Test
    public void shouldFindMinIndexTestCase1() {
        int arr[] = {5,4,3,2,1};
        int pos = 0;
        int minIndex = SelectionSotfApp.findMinIndex(arr,pos);
        assertEquals(4, minIndex);
    }
    @Test
    public void shouldFindMinIndexTestCase2() {
        int arr[] = {1,4,3,2,5};
        int pos = 2;
        int minIndex = SelectionSotfApp.findMinIndex(arr,pos);
        assertEquals(3, minIndex);
    }
    @Test
    public void shouldFindMinIndexTestCase3() {
        int arr[] = {1,2,3,4,5};
        int pos = 2;
        int minIndex = SelectionSotfApp.findMinIndex(arr,pos);
        assertEquals(2, minIndex);
    }
    @Test
    public void shouldFindMinIndexTestCase4() {
        int arr[] = {1,1,1,1,1,0,1};
        int pos = 0;
        int minIndex = SelectionSotfApp.findMinIndex(arr,pos);
        assertEquals(5, minIndex);
    }
    @Test
    public void shouldSwapTestCase1() {
        int arr[] = {5,4,3,2,1};
        int expecteds[] = {1,4,3,2,5};
        int first = 0;
        int second = 4;
        SelectionSotfApp.swap(arr,first,second);
        assertArrayEquals(expecteds, arr);
    }
    @Test
    public void shouldSwapTestCase2() {
        int arr[] = {5,4,3,2,1};
        int expecteds[] = {5,4,3,2,1};
        int first = 0;
        int second = 0;
        SelectionSotfApp.swap(arr,first,second);
        assertArrayEquals(expecteds, arr);
    }
    @Test
    public void shouldSelectsortTestCase1() {
        int arr[] = {5,4,3,2,1};
        int sortend[] = {1,2,3,4,5};
        SelectionSotfApp.selectionSotf(arr);
        assertArrayEquals(sortend, arr);
    }
    @Test
    public void shouldSelectsortTestCase2() {
        int arr[] = {1,4,3,2,5};
        int sortend[] = {1,2,3,4,5};
        SelectionSotfApp.selectionSotf(arr);
        assertArrayEquals(sortend, arr);
    }
    @Test
    public void shouldSelectsortTestCase3() {
        int arr[] = {1,3,4,2,5};
        int sortend[] = {1,2,3,4,5};
        SelectionSotfApp.selectionSotf(arr);
        assertArrayEquals(sortend, arr);
    }
}
