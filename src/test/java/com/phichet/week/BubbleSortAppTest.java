package com.phichet.week;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;
public class BubbleSortAppTest {
    @Test
    public void shouldBubbletestcase1(){
        int arr[] = {5,4,3,2,1};
        int expecteds[] = {4,3,2,1,5};
        int first = 0;
        int second = 4;
        BubbleSortApp.bubble(arr,first,second);
        assertArrayEquals(expecteds, arr);
    }
    @Test
    public void shouldBubbletestcase2(){
        int arr[] = {4,3,2,1,5};
        int expecteds[] = {3,2,1,4,5};
        int first = 0;
        int second = 3;
        BubbleSortApp.bubble(arr,first,second);
        assertArrayEquals(expecteds, arr);
    }
    @Test
    public void shouldBubbletestcase3(){
        int arr[] = {3,2,1,4,5};
        int expecteds[] = {2,1,3,4,5};
        int first = 0;
        int second = 2;
        BubbleSortApp.bubble(arr,first,second);
        assertArrayEquals(expecteds, arr);
    }
    @Test
    public void shouldBubbletestcase4(){
        int arr[] = {2,1,3,4,5};
        int expecteds[] = {1,2,3,4,5};
        int first = 0;
        int second = 1;
        BubbleSortApp.bubble(arr,first,second);
        assertArrayEquals(expecteds, arr);
    }
    @Test
    public void shouldBubbleSorttestcase1(){
        int arr[] = {5,4,3,2,1};
        int expecteds[] = {1,2,3,4,5};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(expecteds, arr);
    }
    @Test
    public void shouldBubbleSortTestCase2() {
        int arr[] = {1,4,3,2,5};
        int sortend[] = {1,2,3,4,5};
        SelectionSotfApp.selectionSotf(arr);
        assertArrayEquals(sortend, arr);
    }
    @Test
    public void shouldBubbleSortTestCase3() {
        int arr[] = {1,3,4,2,5};
        int sortend[] = {1,2,3,4,5};
        SelectionSotfApp.selectionSotf(arr);
        assertArrayEquals(sortend, arr);
    }
}
